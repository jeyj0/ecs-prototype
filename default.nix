let
  pkgs = import ./nix/packages.nix {};
in
  pkgs.stdenv.mkDerivation {
    name = "typeable-test";

    src = ./.;

    buildPhase = ''
      make build
    '';

    installPhase = ''
      mkdir -p $out/bin
      cp out/main $out/bin/typeable-test
    '';

    buildInputs = with pkgs; [
      ghc
      gnumake
    ];
  }

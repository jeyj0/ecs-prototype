hpkgs: with hpkgs; _: [
  generic-lens
  record-hasfield
  aeson
  unordered-containers

  hspec
  QuickCheck
]

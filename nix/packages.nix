{ sources ? import ./sources.nix }:
let
  haskellDeps = import ../dependencies.nix;
  compiler = "ghc8104";

  haskellPackagesOverrides = self: super: {
   # mkDerivation = drv: super.mkDerivation (drv // { jailbreak = true; doHaddock = false;});
  };

  overlay = self: super: {
    niv = (import sources.niv {}).niv;

    haskellPackages = self.haskell.packages.${compiler}.override {
      overrides = haskellPackagesOverrides;
    };
    ghc = self.haskell.packages.${compiler}.ghcWithPackages (haskellDeps self.haskellPackages);

    haskell-language-server = self.haskell.packages.${compiler}.haskell-language-server;
  };
in
  import sources.nixpkgs {
    overlays = [ overlay ];
    config = {};
  }

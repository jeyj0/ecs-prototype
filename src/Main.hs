{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE DeriveDataTypeable        #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE KindSignatures            #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE ImplicitParams            #-}
{-# LANGUAGE LambdaCase                #-}
module Main where

import Control.DeepSeq
import Control.Exception
import Control.Monad
import Data.Maybe
import Data.Functor
import Data.Text hiding (head, map)
import Prelude hiding (putStrLn)
import GHC.Generics
import GHC.Records.Compat
import Data.Aeson
import Data.HashMap.Lazy hiding (map, mapMaybe)

data Record1 = Record1
  { name :: Text
  } deriving (Generic)

instance HasField "name" Record1 Text where
  hasField rec1 = (setter, value)
    where
      value = name rec1
      setter newVal = Record1
        { name = newVal
        }

data Record2 = Record2
  { name' :: Text
  , field2' :: Text
  } deriving (Generic)

instance HasField "name" Record2 Text where
  hasField rec2 = (setter, value)
    where
      value = name' rec2
      setter newVal = Record2
        { name' = newVal
        , field2' = field2' rec2
        }

instance HasField "field2" Record2 Text where
  hasField rec2 = (setter, value)
    where
      value = field2' rec2
      setter newVal = Record2
        { name' = name' rec2
        , field2' = newVal
        }

data Record3 = Record3
  { name'' :: Text
  , field2'' :: Text
  , field3'' :: Text
  } deriving (Generic)

instance HasField "name" Record3 Text where
  hasField rec3 = (setter, value)
    where
      value = name'' rec3
      setter newVal = Record3
        { name'' = newVal
        , field2'' = field2'' rec3
        , field3'' = field3'' rec3
        }

instance HasField "field2" Record3 Text where
  hasField rec3 = (setter, value)
    where
      value = field2'' rec3
      setter newVal = Record3
        { name'' = name'' rec3
        , field2'' = newVal
        , field3'' = field3'' rec3
        }

instance HasField "field3" Record3 Text where
  hasField rec3 = (setter, value)
    where
      value = field3'' rec3
      setter newVal = Record3
        { name'' = name'' rec3
        , field2'' = field2'' rec3
        , field3'' = newVal
        }

-- getAText 
--   :: forall r
--    . ( Typeable r
--      , HasField "name" r Text
--      )
--   => r -> Text
-- getAText record = case cast record :: Maybe Record2 of
--   Nothing -> getField @"name" record
--   Just record2 -> getField @"name" record2 <> getField @"field2" record2
getAText :: Entity -> Text
getAText entity = case getMaybe @Name entity of
  Nothing -> "No Name"
  Just (Name name) -> name

type Entity = HashMap Text Value

class RetrievesComponents e where
  getComponent :: e -> Text -> Maybe Value

class SavesComponents e e' where
  insertComponent :: Text -> Value -> e -> e'

instance SavesComponents Entity Entity where
  insertComponent = insert

instance RetrievesComponents Entity where
  getComponent = (!?)

class HasComponent e c

instance HasComponent Entity c

class Component c where
  identifier :: Text
  serialize :: c -> Value
  deserialize :: Value -> Maybe c

getMaybe
  :: forall c e
   . ( Component c
     , RetrievesComponents e
     )
  => e -> Maybe c
getMaybe entity = case getComponent entity (identifier @c) of
  Nothing -> Nothing
  Just value -> deserialize @c value

get
  :: forall c e
   . ( Component c
     , RetrievesComponents e
     , HasComponent e c
     )
  => e -> c
get entity = fromJust . deserialize @c . fromJust $ getComponent entity (identifier @c)

set
  :: forall c e e'
   . ( Component c
     , SavesComponents e e'
     , HasComponent e' c
     )
  => c -> e -> e'
set component = insertComponent (identifier @c) $ serialize @c component

newtype Name = Name Text

instance Component Name where
  identifier = "Name"
  serialize (Name name) = String name
  deserialize (String name) = Just $ Name name
  deserialize _ = Nothing

newtype Field2 = Field2 Text

instance Component Field2 where
  identifier = "Field2"
  serialize (Field2 field2) = String field2
  deserialize (String field2) = Just $ Field2 field2
  deserialize _ = Nothing

maybeModifyName
  :: forall e
   . ( HasComponent e Name
     , RetrievesComponents e
     , SavesComponents e e
     )
  => UpdateSystem e e
maybeModifyName entity =
  let
    (Name nameValue) = get @Name entity
  in
  Just $ case getMaybe @Field2 entity of
    Nothing -> entity
    Just (Field2 field2Value) -> set @Name (Name $ nameValue <> field2Value) entity

assignName
  :: forall e e'
   . ( SavesComponents e e'
     , HasComponent e' Name
     )
  => Text -> UpdateSystem e e'
assignName name = Just . set @Name (Name name)

type UpdateSystem e e' = (SavesComponents e e') => e -> Maybe e'

applySystem :: UpdateSystem Entity Entity -> [Entity] -> IO [Entity]
applySystem updateSystem entities =
  catMaybes <$> mapM updateSystem' entities
  where
    updateSystem' :: Entity -> IO (Maybe Entity)
    updateSystem' entity = catch (evaluate . force $ updateSystem entity) $ \(_ :: SomeException) -> pure $ Just entity

loadingSystem :: IO [Entity]
loadingSystem = pure
  [ fromList []
  , fromList [("Field2", String "field2")]
  , fromList [("Name", String "I am a name")]
  , fromList [("Name", String "I am a name"), ("Field2", String "field two for task with name")]
  ]

main :: IO ()
main = do
  entities <- loadingSystem
  resultingEntities <- applySystem (maybeModifyName >=> maybeModifyName) entities
  print resultingEntities
